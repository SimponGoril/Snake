from point import Point
import random

class Apple:
    def __init__(self):
        self.color = [255, 0, 0]
        self.body = Point(1, 1, self.color)
    def randomCoordinate(self):
        return random.randint(0, 7)
    def relocate(self):
        newPosition = Point(self.randomCoordinate(), self.randomCoordinate(), self.color)
        if newPosition == self.body:
            self.relocate()
        else:
            self.body = newPosition
