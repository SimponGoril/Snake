from sense_hat import SenseHat
from point import Point
from apple import Apple
from snake import Snake
import time
import animations
sense = SenseHat()
sense.low_light = True

def newGame():
    snake = Snake()
    apple = Apple()

    while True:
        #set movement direction
        time.sleep(0.4)
        snake.moveSnake(apple)

        #paint snake and apple
        sense.clear()
        for point in snake.body:
            sense.set_pixel(point.x, point.y, point.color)
        sense.set_pixel(apple.body.x, apple.body.y, apple.color)

        #end game loop
        if snake.state == 'dead':
            time.sleep(1)
            sense.clear()
            sense.show_message(str(len(snake.body) - 3), text_colour=[150, 150, 150], scroll_speed=0.1)
            break

#public static method main(String... args)
animations.welcomeScreen()
while True:
    newGame()
    animations.restartCountdown()

    #press middle button to exit game
    if(sense.stick.get_events()[-1].direction == 'middle'):
        sense.clear() 
        break
