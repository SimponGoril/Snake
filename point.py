class Point:
    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.color = color
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
