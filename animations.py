from sense_hat import SenseHat
import time
import random
sense = SenseHat()
sense.low_light = True
SPEED = 0.5

O = [0, 0, 0]
R = [255, 0, 0]
G = [0, 100, 0]
BG = [0, 255, 0]
B = [0, 0, 255]
Y = [255, 255, 0]
C = [0, 255, 255]
P = [255, 0, 255]
W = [255, 255, 255]
LW = [100, 100, 100]

letterS = [
G, G, G, G, G, G, G, G,
G, G, W, W, W, W, G, G,
G, G, W, G, G, G, G, G,
G, G, G, W, G, G, G, G,
G, G, G, G, W, G, G, G,
G, G, G, G, G, W, G, G,
G, G, W, W, W, W, G, G,
G, G, G, G, G, G, G, G
]

letterN = [
G, G, G, G, G, G, G, G,
G, G, W, G, G, W, G, G,
G, G, W, G, G, W, G, G,
G, G, W, W, G, W, G, G,
G, G, W, G, W, W, G, G,
G, G, W, G, G, W, G, G,
G, G, W, G, G, W, G, G,
G, G, G, G, G, G, G, G
]

letterA = [
G, G, G, G, G, G, G, G,
G, G, G, W, W, G, G, G,
G, G, W, G, G, W, G, G,
G, G, W, G, G, W, G, G,
G, G, W, W, W, W, G, G,
G, G, W, G, G, W, G, G,
G, G, W, G, G, W, G, G,
G, G, G, G, G, G, G, G
]

letterK = [
G, G, G, G, G, G, G, G,
G, G, W, G, G, W, G, G,
G, G, W, G, W, G, G, G,
G, G, W, W, G, G, G, G,
G, G, W, W, G, G, G, G,
G, G, W, G, W, G, G, G,
G, G, W, G, G, W, G, G,
G, G, G, G, G, G, G, G
]

letterE = [
G, G, G, G, G, G, G, G,
G, G, W, W, W, W, G, G,
G, G, W, G, G, G, G, G,
G, G, W, W, W, W, G, G,
G, G, W, G, G, G, G, G,
G, G, W, G, G, G, G, G,
G, G, W, W, W, W, G, G,
G, G, G, G, G, G, G, G
]

letterGO = [
LW, LW, LW, LW, LW, LW, LW, LW,
LW, BG, BG, LW, LW, BG, LW, R,
BG, LW, LW, LW, BG, LW, BG, R,
BG, LW, LW, LW, BG, LW, BG, R,
BG, BG, BG, LW, BG, LW, BG, R,
BG, LW, BG, LW, BG, LW, BG, LW,
LW, BG, BG, LW, LW, BG, LW, R,
LW, LW, LW, LW, LW, LW, LW, LW
]

GREEN = [
G, G, G, G, G, G, G, G,
G, G, G, G, G, G, G, G,
G, G, G, G, G, G, G, G,
G, G, G, G, G, G, G, G,
G, G, G, G, G, G, G, G,
G, G, G, G, G, G, G, G,
G, G, G, G, G, G, G, G,
G, G, G, G, G, G, G, G
]

def welcomeScreen():
    sense.set_pixels(GREEN)
    time.sleep(SPEED)
    sense.set_pixels(letterS)
    time.sleep(SPEED)
    sense.set_pixels(letterN)
    time.sleep(SPEED)
    sense.set_pixels(letterA)
    time.sleep(SPEED)
    sense.set_pixels(letterK)
    time.sleep(SPEED)
    sense.set_pixels(letterE)
    time.sleep(SPEED)
    sense.set_pixels(letterGO)
    time.sleep(SPEED + SPEED)
    
def restartCountdown():
    sense.show_message(str(3), text_colour=LW, scroll_speed=0.05)

    sense.show_message(str(2), text_colour=LW, scroll_speed=0.05)
 
    sense.show_message(str(1), text_colour=LW, scroll_speed=0.05)


def go():
    sense.set_pixels(letterGO)
    time.sleep(1)
