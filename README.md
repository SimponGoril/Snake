# SNAKE

Simple game for [Raspberry Pi Sense Hat](https://www.raspberrypi.org/products/sense-hat/) written in Python

[![Snake - demo](http://img.youtube.com/vi/E0eSixPusN0/0.jpg)](http://www.youtube.com/watch?v=E0eSixPusN0)

*(click on image for full video)*

---

## How to install
Fire up Raspbian terminal and clone source files
```
git clone https://github.com/SimponGoril/Snake.git
```
Then simply execute game.py file in python runtime
```
python game.py
```

### Pro-tip: Run snake game without Sense Hat with [trinket emulator](https://www.raspberrypi.org/blog/sense-hat-emulator/)!
