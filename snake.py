from sense_hat import SenseHat
from apple import Apple
from point import Point
sense = SenseHat()

class Snake:
    def __init__(self):
        self.color = [0, 255, 0]
        self.body = [Point(4, 4, self.color), Point(3, 4, self.color), Point(2, 4, self.color)]
        self.state = 'left'
    def moveSnake(self, apple):
        newHead = ''
        joystick = sense.stick.get_events();
        if len(joystick) > 0 and joystick[0].direction != 'middle':
            self.state = joystick[0].direction
        oldHead = self.body[-1]
        
        if self.state == 'right': 
            newX = oldHead.x + 1 if oldHead.x < 7 else 0
            newHead = Point(newX, oldHead.y, self.color)
        elif self.state == 'left':
            newX = oldHead.x - 1 if oldHead.x > 0 else 7
            newHead = Point(newX, oldHead.y, self.color)
        elif self.state == 'up':
            newY = oldHead.y - 1 if oldHead.y > 0 else 7
            newHead = Point(oldHead.x, newY, self.color)
        elif self.state == 'down':
            newY = oldHead.y + 1 if oldHead.y < 7 else 0
            newHead = Point(oldHead.x, newY, self.color)

        #collision detection
        if newHead in self.body:
            for point in self.body:
                point.color = [255, 0, 0]
            self.state = 'dead'
        elif apple.body == newHead:
            self.body.append(newHead)
            apple.relocate()
            while apple.body in self.body:
                apple.relocate()
        else:
            self.body.append(newHead)
            self.body.pop(0)
